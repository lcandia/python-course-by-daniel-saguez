--Para actualizar las estadísticas de una tabla y todos sus índices, se debe ejecutar la siguiente sentencia:
ANALYZE TABLE KNOWLEDGE.T_REP_ACT_005 COMPUTE STATISTICS;

--Para actualizar las estadísticas unicamente de la tabla y no de los índices, ejecutar:
ANALYZE TABLE KNOWLEDGE.T_REP_ACT_005 COMPUTE STATISTICS FOR TABLE;

--Para actualizar las estadísticas de los índices:
ANALYZE TABLE KNOWLEDGE.T_REP_ACT_005 COMPUTE STATISTICS FOR ALL INDEXES;

http://ora.u440.com/dba/analyze.html
http://mioracle.blogspot.com/2008/02/actualizar-las-estadsticas.html